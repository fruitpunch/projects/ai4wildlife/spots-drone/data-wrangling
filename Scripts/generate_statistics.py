
import json
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from seaborn.axisgrid import jointplot
import joblib
import collections
sns.set()

def main(annotation_path,dataset_name,single=True):

    # reading input annotations file 
    with open(annotation_path,'r') as fh :
        input_content = json.load(fh)

    areas = []
    medium_areas = []
    big_areas = []

    lila_persons = []
    labeled_persons = []

    count_dict = {}

    for annotation in input_content['annotations']:
        areas.append(annotation['area'])

        if annotation['area'] > 5000:
            big_areas.append(annotation['image_id'])

        if annotation['area'] > 1500:
            medium_areas.append(annotation['image_id'])

        if annotation['image_id'] in count_dict.keys():
            count_dict[annotation['image_id']] += 1
        else:
            count_dict[annotation['image_id']] = 1

        if count_dict[annotation['image_id']] >= 5:
            if dataset_name == 'lila':
                lila_persons.append(annotation['image_id'])

            elif dataset_name == 'labeled':
                labeled_persons.append(annotation['image_id'])

            count_dict[annotation['image_id']] = 0

    # sns.histplot(areas,alpha=alpha,label=dataset_name,bins=30)
    # plt.title(f'Average bounding box size for {dataset_name} dataset')
    # plt.xlabel('Area size')
    # plt.savefig(f'{dataset_name}_bbox.png')
    # plt.clf() 

    if dataset_name == 'lila':
        joblib.dump(big_areas,'big_areas.pkl')
        joblib.dump(medium_areas,'medium_areas.pkl')
        joblib.dump(lila_persons,'lila_persons.pkl')

    elif dataset_name == 'labeled':
        joblib.dump(labeled_persons,'labeled_persons.pkl')


if __name__ == '__main__':
    
    datasets = ['aachen','lila','labeled']
    
    for dataset in datasets:
        print(dataset)
        main(f'annotations/{dataset}_annotations.json',dataset)

