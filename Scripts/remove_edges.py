import json
import joblib
import numpy as np

def main(path,width,height):

    # reading input annotations file 
    with open(f'annotations/{path}','r') as fh :
        input_content = json.load(fh)

    keep = []
    removed = 0

    for i, annotation in enumerate(input_content['annotations']):
        bbox = annotation['bbox']

        x = bbox[0]
        y = bbox[1]
        width_x = bbox[2]
        height_y = bbox[3]

        if x < 0 or x + width_x > width:
            removed += 1
            continue

        elif y < 0 or y + height_y > height:
            removed += 1
            continue   

        else:
            keep.append(i)

    input_content['annotations'] = list(np.array(input_content['annotations'])[keep])
    joblib.dump(input_content,f'{path[:-5]}_cleaned.json')

    with open(f'{path[:-5]}_cleaned.json', 'w') as fh :
        json.dump(input_content , fh, indent=4)

    print(f'{removed} bounding boxes removed')

if __name__ == '__main__':
    to_clean = ['aachen_annotations.json','lila_annotations.json','instances_train.json','instances_test.json','instances_val.json']

    for path in to_clean:
        main(path,640,470)