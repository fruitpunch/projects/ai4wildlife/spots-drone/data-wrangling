# Run as python visualize_bboxes.py 
# Python 3.x is used 
# program to visualize the bounding boxe annotations for a given image 
# input parameters are input image path (including folder location or relative path), input image name, 
# output image path 
# path to the simplified annotations json file 
# image name in the input parameter and annotations file must match 
# make sure that the correct pair of image and annotation file is provided. So , if a training image is used , then training annotations file must be provided

# importing required libraries 
import cv2 
import json
import random
import joblib 

# defining input parameters 
# image_filepath         = 'test_images' # input image filepath including folder(or relative filepath) and filename  
dataset = 'lila'
image_filepath = '../Data/Conservation resized'
simple_annots_filepath = f'annotations/{dataset}_annotations_simplified.json'
annots_filepath = f'annotations/{dataset}_annotations.json'
output_filepath = 'test_images'

# loading simple annotations json file 
with open(annots_filepath, 'r') as fh :
    original_content = json.load(fh)

# loading simple annotations json file 
with open(simple_annots_filepath, 'r') as fh :
    content = json.load(fh)

medium_areas = joblib.load('medium_areas.pkl')
big_areas = joblib.load('big_areas.pkl')

images_ids = []
image_filenames =  [] # just the input image filename . manually provided this to avoid parsing filepath which may have OS dependencies

for ids in [medium_areas]:
    for i in range(5):
        images_ids.append(random.choice(ids))

for image in original_content['images']:
    if image['id'] in images_ids:
        image_filenames.append(image['file_name'])

# image_filenames         = ['0000000067_0000000029_0000000544.jpg','0000000067_0000000045_0000000662.jpg','0000000351_0000000000_0000000021.jpg','0000000367_0000000000_0000004618.jpg'] # just the input image filename . manually provided this to avoid parsing filepath which may have OS dependencies


for image_filename in image_filenames:
    annotations = content['annotations']
    found = False
    for filename, values_dict in annotations.items():

        if filename == image_filename :
            found = True 
            bboxes = values_dict['bboxes']

    if found is False :
        print(f"filename {image_filename} not found in annotations file. Exiting")
        continue
        # exit()

    image = cv2.imread(f'{image_filepath}/{image_filename}')
    # print("image(numpy array) shape : {}".format(image.shape))
    for bbox in bboxes :
        top_left = [int(bbox[0]), int(bbox[1])] # x,y coordinates of top left corner of the bounding box 
        bottom_right = [ int(bbox[0] + bbox[2]) , int(bbox[1] + bbox[3]) ]
        cv2.rectangle(image, top_left, bottom_right, (0,0,255), 1)
    cv2.imwrite(f'{output_filepath}/annotated_{image_filename}', image)

