# define data paths
aachen_path = '../Data/Aachen Dataset'
images_path = '../Data/Labeld data/images'
annotations_path = '../Data/Labeld data/annotations'
resized_path = '../Data/Aachen resized'
cropped_path = '../Data/Aachen cropped'
mode = 'crop'

import json

# load current annotations, used for formatting later
with open(annotations_path + '/instances_val.json','r') as f:
    input_content = json.load(f)
    
# aachen format: 1024 x 576
# current format: 640 x 470

# first step:  resize aachen images to 640x470
# second step: create annotations for aachen dataset

# goal: resize 1024x576 format to 640x470
import cv2

# functions
def resize(width, height):
    '''Reads image and changes the format using cv2.resize, requires paths as global variables'''
    img_array = cv2.imread(original_path)
    new_array = cv2.resize(img_array, (width, height), interpolation=cv2.INTER_CUBIC)
    cv2.imwrite(output_path, new_array)

def crop(width,height):
    '''Reads image and crops to specified width and height, requires paths as global variables
    
    returns offset width and height, this is required for adjusting the bounding boxes'''

    image = cv2.imread(original_path)
    image_h, image_w = image.shape[0], image.shape[1]
    offset_h = int((image_h - height)//2)
    offset_w = int((image_w - width)//2)
    image = image[offset_h:offset_h + height, offset_w:offset_w + width]             # center cropping the image to match required output width and height 
    cv2.imwrite(output_path, image)

    return image_h, image_w

def add_image(annotations,image_id,file_name):
    '''This function adds an image to the annotations dict according to the specified parameters'''
    
    annotations['images'].append({
                            'id': image_id,
                            'width': width,
                            'height': height,
                            'file_name': file_name,
                            'license': 0,
                            'flickr_url': '',
                            'coco_url': '',
                            'date_captured': 0
                        })   

    
def add_annotation(annotations,annotation_id,image_id,coordinates):
    '''This function adds an image annotation to the annotations dict according to the specified parameters'''
    
    annotations['annotations'].append({
                                'id': annotation_id,
                                'image_id': image_id,
                                'category_id': 1,
                                'segmentation': [],
                                'area': coordinates[2] * coordinates[3],
                                'bbox': coordinates,
                                'iscrowd': 0,
                                'attributes': {'occluded': False, 'track_id': 0, 'keyframe': True}
                            })

import os

# list all files in aachen dataset folder
files = os.listdir(aachen_path)

# keep only images 
images = [file for file in files if ('.jpg' in file)]

# resize all images
# print('Resizing or cropping ...')
# for image in images:
    
#     original_path = f'{aachen_path}/{image}'
#     output_path = f'{cropped_path}/{image}'
    
#     # resize(640,470)
#     crop(640,470)

# After resizing, create initial dictionary for format, everything is the same as the original format except for the images and annotations
aachen_annotations = {'licenses' : input_content['licenses'], 'info' : input_content['info'], 'categories' : input_content['categories'], 'images' : [], 'annotations' : []}

# list all images
# images = os.listdir(resized_path)

# initialize ids
image_id = 100001
annotation_id = 100001
width = 640 
height = 470

images_with_persons = 0
images_with_cars = 0
n_persons = 0
n_cars = 0

# loop over images
for image in images:

    original_path = f'{aachen_path}/{image}'

    # resize or crop images
    if mode == 'crop':
        output_path = f'{cropped_path}/{image}'    
        source_height, source_width = crop(width,height)

    elif mode == 'resize':
        output_path = f'{resized_path}/{image}'    
        resize(width,height)

    person_in_image = False # for counting
    car_in_image = False

    # add image to annotations dictionary, the id starts at 100 000
    add_image(aachen_annotations,image_id,image)
    
    # get path of corresponding .txt file
    annotation_path = f"{aachen_path}/{image.replace('.jpg','.txt')}"
    
    # read annotation file
    try:
        with open(annotation_path) as f:
            annotations = f.readlines()
    except FileNotFoundError: # occurs when there is no labeled object
        continue
            
    # loop over annotation and add
    for annotation in annotations:
        
        # clean from newline and split into coordinates
        annotation = annotation.strip()
        coordinates = annotation.split(' ')[1:]

        if mode == 'resize':
           
            # transform to pixel values
            coordinates = [(float(coordinate) * width) if (i in {0,2}) else coordinate for i, coordinate in enumerate(coordinates)] # x values
            coordinates = [(float(coordinate) * height) if (i in {1,3}) else coordinate for i, coordinate in enumerate(coordinates)] # y values

            # change center value to top left
            coordinates[0] = coordinates[0] - 0.5 * coordinates[2] # x value
            coordinates[1] = coordinates[1] - 0.5 * coordinates[3] # y 
            
        elif mode == 'crop':

            # transform to source pixel values
            coordinates = [(float(coordinate) * source_width) if (i in {0,2}) else coordinate for i, coordinate in enumerate(coordinates)] # x values
            coordinates = [(float(coordinate) * source_height) if (i in {1,3}) else coordinate for i, coordinate in enumerate(coordinates)] # y values

            # change center value to top left
            coordinates[0] = coordinates[0] - 0.5 * coordinates[2] # x value
            coordinates[1] = coordinates[1] - 0.5 * coordinates[3] # y 

            # use offset to shift bounding boxes
            offset_h = int((source_height - height)//2)
            offset_w = int((source_width - width)//2)

            # 
            coordinates[0] = coordinates[0] - offset_w # x value
            coordinates[1] = coordinates[1] - offset_h # y 

        if image == '029103.jpg':
            print(coordinates)
            print(annotation)

        # if annotation describes a person, add the annotations
        if annotation[0] in ['1']:
            # print(image)
            add_annotation(aachen_annotations,annotation_id,image_id,coordinates)
            annotation_id += 1

            person_in_image = True
            n_persons += 1

        elif annotation[0] in ['0']:
            # add_annotation(aachen_annotations,annotation_id,image_id,coordinates)
            car_in_image = True
            n_cars += 1

    image_id += 1

    if person_in_image:
        images_with_persons += 1

    if car_in_image:
        images_with_cars += 1

print(f'Total number of images {len(images)}')
print(f'Images with persons {images_with_persons}')
print(f'Images with cars {images_with_cars}')
print(f'Number of persons {n_persons}')
print(f'Number of cars {n_cars}')

# save 
with open('annotations/aachen_annotations.json', 'w') as fh :
    json.dump(aachen_annotations , fh, indent=4)

print('[DONE]')