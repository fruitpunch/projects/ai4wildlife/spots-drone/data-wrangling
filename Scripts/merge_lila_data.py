import pandas as pd
from pandas.tseries import frequencies

# define data paths
conservation_path = '../Datasets/conservation_drones_train_real/TrainReal/images'
images_path = '../Data/Labeld data/images'
coco_path = '../Data/Labeld data/annotations'
resized_path = '../Data/Conservation resized'
conservation_annotations_path = '../Datasets/conservation_drones_train_real/TrainReal/annotations'

frequency = 1
resize = True

import json

# load current annotations, used for formatting later
with open(coco_path + '/instances_val.json','r') as f:
    input_content = json.load(f)


# first step:  resize aachen images to 640x470
# second step: create annotations for aachen dataset

# goal: resize 1024x576 format to 640x470
import cv2

# functions
def resize(width, height):
    '''Reads image and changes the format using cv2.resize, requires paths as global variables'''
    img_array = cv2.imread(original_path)
    image_h, image_w = img_array.shape[0], img_array.shape[1]
    new_array = cv2.resize(img_array, (width, height), interpolation=cv2.INTER_CUBIC)
    cv2.imwrite(output_path, new_array)
    return image_h, image_w

def add_image(annotations,image_id,file_name):
    '''This function adds an image to the annotations dict according to the specified parameters'''
    
    annotations['images'].append({
                            'id': image_id,
                            'width': width,
                            'height': height,
                            'file_name': file_name,
                            'license': 0,
                            'flickr_url': '',
                            'coco_url': '',
                            'date_captured': 0
                        })   

    
def add_annotation(annotations,annotation_id,image_id,coordinates,occluded):
    '''This function adds an image annotation to the annotations dict according to the specified parameters'''
    
    annotations['annotations'].append({
                                'id': annotation_id,
                                'image_id': image_id,
                                'category_id': 1,
                                'segmentation': [],
                                'area': coordinates[2] * coordinates[3],
                                'bbox': coordinates,
                                'iscrowd': 0,
                                'attributes': {'occluded': bool(occluded), 'track_id': 0, 'keyframe': True}
                            })

def to_frame_number(image_name):

    number = image_name.split('.')[0].split('_')[-1]

    for i, character in enumerate(number):
        if character != '0':
            number = number[i:]
            break

    return int(number)

import os

# list all files in aachen dataset folder
folders = os.listdir(conservation_path)

conservation_annotations = {'licenses' : input_content['licenses'], 'info' : input_content['info'], 'categories' : input_content['categories'], 'images' : [], 'annotations' : []}

n_images = 0
images_with_persons = 0
images_with_animals = 0
n_persons = 0
n_animals = 0

# initialize ids
image_id = 200001
annotation_id = 200001
width = 640 
height = 470

for folder in folders:

    path = f'{conservation_path}/{folder}'

    images = os.listdir(path)
    all_image_annotations = pd.read_csv(f'{conservation_annotations_path}/{folder}.csv',header=None)
    all_image_annotations.columns = ['frame_number', 'object_id', 'x', 'y', 'w', 'h', 'class', 'species', 'occlusion', 'noise']


    # loop over images, taking steps defined by frequency
    for image_index in range(0,len(images),frequency):

        image = images[image_index]
        
        # resize
        original_path = f'{path}/{image}'
        output_path = f'{resized_path}/{image}'    
        source_height, source_width = resize(width,height)

        person_in_image = False # for counting
        animal_in_image = False

        # add image to annotations dictionary, the id starts at 100 000
        add_image(conservation_annotations,image_id,image)
    
        frame_number = to_frame_number(image)

        image_annotations = all_image_annotations[all_image_annotations['frame_number'] == frame_number]

        # if no annotations are found
        if not any(image_annotations.index):
            continue
    
        # loop over annotation and add
        for i in range(len(image_annotations)):
            
            coordinates = list(image_annotations.iloc[i][['x','y','w','h']])

            # transform to pixel values
            coordinates = [(coordinate / source_width) if (i in {0,2}) else coordinate for i, coordinate in enumerate(coordinates)] # x values
            coordinates = [(coordinate / source_height) if (i in {1,3}) else coordinate for i, coordinate in enumerate(coordinates)] # y values
            
            # transform to pixel values
            coordinates = [(coordinate * width) if (i in {0,2}) else coordinate for i, coordinate in enumerate(coordinates)] # x values
            coordinates = [(coordinate * height) if (i in {1,3}) else coordinate for i, coordinate in enumerate(coordinates)] # y values

            # if annotation describes a person, add the annotations
            if image_annotations.iloc[i]['class'] in [1]:

                occluded = image_annotations.iloc[i]['occlusion']
                add_annotation(conservation_annotations,annotation_id,image_id,coordinates,occluded=occluded)
                annotation_id += 1

                person_in_image = True
                n_persons += 1

            elif image_annotations.iloc[i]['class'] in [0]:
                occluded = image_annotations.iloc[i]['occlusion']
                # add_annotation(conservation_annotations,annotation_id,image_id,coordinates,occluded=occluded)
                
                animal_in_image = True
                n_animals += 1

        image_id += 1

        if person_in_image:
            images_with_persons += 1

        if animal_in_image:
            images_with_animals += 1

    n_images += len(images)

print(f'Total number of images {n_images}')
print(f'Images with persons {images_with_persons}')
print(f'Images with animals {images_with_animals}')
print(f'Number of persons {n_persons}')
print(f'Number of animals {n_animals}')


# save 
with open('annotations/lila_annotations.json', 'w') as fh :
    json.dump(conservation_annotations , fh, indent=4)

print('[DONE]')