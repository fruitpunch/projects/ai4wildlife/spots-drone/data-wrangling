# Run as -> python create_simple_annot.py 
# Python 3.x is used
# program for creating a simplied version of the annotations json file containing bounding box annotations 
# input is the original annotations file ( https://drive.google.com/drive/folders/1mn_wonQshmps9MLGkywPLaCC3iB45Uow )
# output is a simplified annotations file 
# refer to 'InfoDataAnnotations' google doc file for more details  

# importing required libraries 
import json 

dataset = 'lila'

# defining input and output filepaths for the annotations file 
input_filepath  = f'annotations/{dataset}_annotations.json'             # absolute or relative path to the input annotations file
output_filepath = f'annotations/{dataset}_annotations_simplified.json'  # absolute or relative path to the output simplified annotations file 

# reading input annotations file 
with open(input_filepath,'r') as fh :
    input_content = json.load(fh)

# print(input_content['images'][0])
# print(input_content['annotations'][0])

# for image in input_content['images']:

#     if image['file_name'] == '000488.jpg':
#         print(image)
#         test_id = image['id']

# for annotation in input_content['annotations']:

#     if annotation['image_id'] == test_id:
#         print('found')
#         print(annotation)



# creating dictionary for storing output annotations
output_content = dict()
for key in ['licenses', 'info', 'categories'] :
    output_content[key] = input_content[key]  # copying metadata info as is 

# creating a dictionary named image_ids where key is image_id and value is another dictionary 
# value dictionary is of the format {'filename':filename , 'bboxes':[bbox1, bbox2, ...]} 
# where each bbbox is a list of bounding box coordinates 
image_ids_dict = dict()
for image_info in input_content['images']:
    image_id = image_info['id']
    image_filename = image_info['file_name']
    # checking the assumption that all images are of width 640 pixels and height 470 pixels 
    assert image_info['width']  == 640
    assert image_info['height'] == 470 
    # creating a values_dict for storing image filename and corresponding list of bounding boxes
    values_dict = dict()
    values_dict['filename'] = image_filename
    values_dict['bboxes']   = []  # this list will be updated when reading the annotations key in input_content dicitionary
    # adding key-value pair to image_ids_dict 
    image_ids_dict[image_id] = values_dict

for annot_info in input_content['annotations'] :
    image_id = annot_info['image_id']
    bbox     = annot_info['bbox']
    image_ids_dict[image_id]['bboxes'].append(bbox)

# creating a modified output dictionary where the keys are image filenames and values is a dictionary with image id and bounding boxes list 
output_dict = dict()
for image_id, values_dict  in image_ids_dict.items():
    temp_dict = dict()
    temp_dict['image_id'] = image_id 
    temp_dict['bboxes']   = values_dict['bboxes']
    filename = values_dict['filename']
    output_dict[filename] = temp_dict

# writing annotations info to output_content dictionary 
output_content['annotations'] = output_dict

# print(output_content['annotations']['000488.jpg'])

# writing output_content to output annotations json file 
with open(output_filepath, 'w') as fh :
    json.dump(output_content , fh, indent=4)

print("[DONE]") 

# OPTIONAL for finding filenames with 0 bounding box or more than 3 bounding box 
# for filename, temp_dict in output_dict.items():
#     if len(temp_dict['bboxes']) > 3 or len(temp_dict['bboxes']) == 0:
#         print("{} has {} bounding boxes".format(filename, len(temp_dict['bboxes'])))
